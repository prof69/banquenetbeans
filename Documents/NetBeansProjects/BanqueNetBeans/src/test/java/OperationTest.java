import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import controler.Operation;
import model.Client;

public class OperationTest {
	
	@Test
	public void verserOK() {
		Operation o = new Operation();
		o.initClientCompte();
                
		assertEquals("Solde après opération : 1000000.0€", o.verser("hay", 1000000));
		
	}

}